package com.example.ivan.weargallery

import android.app.Activity
import android.content.Intent
import android.database.MergeCursor
import android.os.AsyncTask
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.GridView
import android.widget.ImageView
import com.bumptech.glide.Glide
import java.io.File
import java.util.*

class AlbumActivity : AppCompatActivity() {

    internal var f: Function = Function()
    internal lateinit var galleryGridView: GridView
    internal var imageList = ArrayList<HashMap<String, String>>()
    internal var album_name = ""
    internal lateinit var loadAlbumTask: LoadAlbumImages


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_album)

        val intent = intent
        album_name = intent.getStringExtra("name")
        title = album_name


        galleryGridView = findViewById<GridView>(R.id.galleryGridView)
        val iDisplayWidth = resources.displayMetrics.widthPixels
        val resources = applicationContext.resources
        val metrics = resources.displayMetrics
        var dp = iDisplayWidth / (metrics.densityDpi / 160f)

        if (dp < 360) {
            dp = (dp - 17) / 2
            val px = f.convertDpToPixel(dp, applicationContext)
            galleryGridView.columnWidth = Math.round(px)
        }


        loadAlbumTask = LoadAlbumImages()
        loadAlbumTask.execute()


    }


    internal inner class LoadAlbumImages : AsyncTask<String, Void, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
            imageList.clear()
        }

        override fun doInBackground(vararg args: String): String {
            val xml = ""

            var path: String?
            var album: String?
            var timestamp: String?
            val uriExternal = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            val uriInternal = android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI

            val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_MODIFIED)

            val cursorExternal = contentResolver.query(uriExternal, projection, "bucket_display_name = \"$album_name\"", null, null)
            val cursorInternal = contentResolver.query(uriInternal, projection, "bucket_display_name = \"$album_name\"", null, null)
            val cursor = MergeCursor(arrayOf(cursorExternal, cursorInternal))
            while (cursor.moveToNext()) {

                path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA))
                album = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME))
                timestamp = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_MODIFIED))

                imageList.add(f.mappingInbox(album, path, timestamp, f.converToTime(timestamp), null.toString()))
            }
            cursor.close()
            Collections.sort(imageList, MapComparator(f.KEY_TIMESTAMP, "dsc")) // Arranging photo album by timestamp decending
            return xml
        }

        override fun onPostExecute(xml: String) {

            val adapter = SingleAlbumAdapter(this@AlbumActivity, imageList)
            galleryGridView.adapter = adapter
            galleryGridView.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
                val intent = Intent(this@AlbumActivity, GalleryPreview::class.java)
                intent.putExtra("path", imageList[+position][f.KEY_PATH])
                startActivity(intent)
            }
        }
    }
}


internal class SingleAlbumAdapter(private val activity: Activity, private val data: ArrayList<HashMap<String, String>>) : BaseAdapter() {

    internal var f = Function()

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        var holder: SingleAlbumViewHolder? = null
        if (convertView == null) {
            holder = SingleAlbumViewHolder()
            convertView = LayoutInflater.from(activity).inflate(
                    R.layout.single_album_row, parent, false)

            holder.galleryImage = convertView!!.findViewById(R.id.galleryImage) as ImageView

            convertView.tag = holder
        } else {
            holder = convertView.tag as SingleAlbumViewHolder
        }
        holder.galleryImage!!.id = position

        var song = HashMap<String, String>()
        song = data[position]
        try {

            Glide.with(activity)
                    .load(File(song[f.KEY_PATH])) // Uri of the picture
                    .into(holder.galleryImage!!)


        } catch (e: Exception) {
        }

        return convertView
    }
}


internal class SingleAlbumViewHolder {
    var galleryImage: ImageView? = null
}