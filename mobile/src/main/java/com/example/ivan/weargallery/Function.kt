package com.example.ivan.weargallery

import android.content.Context
import android.content.pm.PackageManager
import android.database.MergeCursor
import android.graphics.Bitmap
import android.os.Build
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.view.View
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.wearable.Asset
import com.google.android.gms.wearable.DataItem
import com.google.android.gms.wearable.PutDataMapRequest
import com.google.android.gms.wearable.Wearable
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class Function {
    internal val KEY_ALBUM = "album_name"
    internal val KEY_PATH = "path"
    internal val KEY_TIMESTAMP = "timestamp"
    internal val KEY_TIME = "date"
    internal val KEY_COUNT = "date"


    fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }


    fun mappingInbox(album: String, path: String, timestamp: String, time: String, count: String): HashMap<String, String> {
        val map = HashMap<String, String>()
        map[KEY_ALBUM] = album
        map[KEY_PATH] = path
        map[KEY_TIMESTAMP] = timestamp
        map[KEY_TIME] = time
        map[KEY_COUNT] = count
        return map
    }


    fun getCount(c: Context, album_name: String): String {
        val uriExternal = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val uriInternal = android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI

        val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_MODIFIED)
        val cursorExternal = c.contentResolver.query(uriExternal, projection, "bucket_display_name = \"$album_name\"", null, null)
        val cursorInternal = c.contentResolver.query(uriInternal, projection, "bucket_display_name = \"$album_name\"", null, null)
        val cursor = MergeCursor(arrayOf(cursorExternal, cursorInternal))


        return cursor.count.toString() + " Photos"
    }

    fun converToTime(timestamp: String): String {
        val datetime = java.lang.Long.parseLong(timestamp)
        val date = Date(datetime)
        val formatter = SimpleDateFormat("dd/MM HH:mm")
        return formatter.format(date)
    }

    fun convertDpToPixel(dp: Float, context: Context): Float {
        val resources = context.resources
        val metrics = resources.displayMetrics
        return dp * (metrics.densityDpi / 160f)
    }

}