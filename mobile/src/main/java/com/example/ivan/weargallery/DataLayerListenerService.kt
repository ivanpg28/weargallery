package com.example.ivan.weargallery

import android.content.Intent
import android.util.Log
import com.google.android.gms.wearable.MessageEvent
import com.google.android.gms.wearable.WearableListenerService
import com.example.ivan.weargallery.Constants.TAG
import com.example.ivan.weargallery.Constants.WEAR_MESSAGE_PATH

class DataLayerListenerService : WearableListenerService() {

    override fun onMessageReceived(messageEvent: MessageEvent?) {
        if (messageEvent!!.path.equals(WEAR_MESSAGE_PATH, ignoreCase = true)) {

            val text = String(messageEvent.data)
            Log.d(TAG, "Message received from node: $text")

            //start service to download image
            val fetchImageIntent = Intent(this, FetchImageService::class.java)
            startService(fetchImageIntent)
        }
    }

}