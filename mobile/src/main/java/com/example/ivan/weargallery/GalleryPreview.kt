package com.example.ivan.weargallery

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import com.bumptech.glide.Glide
import java.io.File

class GalleryPreview : AppCompatActivity() {

    internal lateinit var GalleryPreviewImg: ImageView
    internal lateinit var path: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.gallery_preview)
        val intent = intent
        path = intent.getStringExtra("path")
        GalleryPreviewImg = findViewById(R.id.GalleryPreviewImg) as ImageView
        Glide.with(this@GalleryPreview)
                .load(File(path)) // Uri of the picture
                .into(GalleryPreviewImg)
    }
}