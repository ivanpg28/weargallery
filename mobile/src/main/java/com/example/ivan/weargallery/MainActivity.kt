package com.example.ivan.weargallery

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.database.Cursor
import android.database.MergeCursor
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.GridView
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

import com.bumptech.glide.Glide

import java.io.File
import java.util.ArrayList
import java.util.Collections
import java.util.HashMap

class MainActivity : AppCompatActivity() {
    var f = Function()
    internal lateinit var loadAlbumTask: LoadAlbum
    internal lateinit var galleryGridView: GridView
    internal var albumList = ArrayList<HashMap<String, String>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        galleryGridView = findViewById<View>(R.id.galleryGridView) as GridView

        val iDisplayWidth = resources.displayMetrics.widthPixels
        val resources = applicationContext.resources
        val metrics = resources.displayMetrics
        var dp = iDisplayWidth / (metrics.densityDpi / 160f)

        if (dp < 360) {
            dp = (dp - 17) / 2
            val px = f.convertDpToPixel(dp, applicationContext)
            galleryGridView.setColumnWidth(Math.round(px))
        }

        val permisos = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        if (!f.hasPermissions(this, *permisos)) {
            ActivityCompat.requestPermissions(this, permisos, REQUEST_PERMISSION_KEY)
        }
    }


    internal inner class LoadAlbum : AsyncTask<String, Void, String>() {
        override fun onPreExecute() {
            super.onPreExecute()
            albumList.clear()
        }

        override fun doInBackground(vararg args: String): String {
            val xml = ""

            var path: String? = null
            var album: String? = null
            var timestamp: String? = null
            var countPhoto: String? = null
            val uriExternal = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            val uriInternal = android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI


            val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_MODIFIED)
            val cursorExternal = contentResolver.query(uriExternal, projection, "_data IS NOT NULL) GROUP BY (bucket_display_name", null, null)
            val cursorInternal = contentResolver.query(uriInternal, projection, "_data IS NOT NULL) GROUP BY (bucket_display_name", null, null)
            val cursor = MergeCursor(arrayOf(cursorExternal, cursorInternal))

            while (cursor.moveToNext()) {

                path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA))
                album = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME))
                timestamp = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_MODIFIED))
                countPhoto = f.getCount(applicationContext, album)

                albumList.add(f.mappingInbox(album, path, timestamp, f.converToTime(timestamp), countPhoto))
            }
            cursor.close()
            Collections.sort(albumList, MapComparator(f.KEY_TIMESTAMP, "dsc")) // Arranging photo album by timestamp decending
            return xml
        }

        override fun onPostExecute(xml: String) {

            val adapter = AlbumAdapter(this@MainActivity, albumList)
            galleryGridView.adapter = adapter
            galleryGridView.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
                val intent = Intent(this@MainActivity, AlbumActivity::class.java)
                intent.putExtra("name", albumList[+position][f.KEY_ALBUM])
                startActivity(intent)
            }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_KEY -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadAlbumTask = LoadAlbum()
                    loadAlbumTask.execute()
                } else {
                    Toast.makeText(this@MainActivity, "You must accept permissions.", Toast.LENGTH_LONG).show()
                }
            }
        }

    }


    override fun onResume() {
        super.onResume()

        val permisos = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
        if (!f.hasPermissions(this, *permisos)) {
            ActivityCompat.requestPermissions(this, permisos, REQUEST_PERMISSION_KEY)
        } else {
            loadAlbumTask = LoadAlbum()
            loadAlbumTask.execute()
        }

    }

    companion object {

        internal val REQUEST_PERMISSION_KEY = 1
    }
}


    internal class AlbumAdapter(private val activity: Activity, private val data: ArrayList<HashMap<String, String>>) : BaseAdapter() {
        var f = Function()
        override fun getCount(): Int {
            return data.size
        }

        override fun getItem(position: Int): Any {
            return position
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView
            var holder: AlbumViewHolder? = null
            if (convertView == null) {
                holder = AlbumViewHolder()
                convertView = LayoutInflater.from(activity).inflate(
                        R.layout.album_row, parent, false)

                holder.galleryImage = convertView!!.findViewById<View>(R.id.galleryImage) as ImageView
                holder.gallery_count = convertView.findViewById<View>(R.id.gallery_count) as TextView
                holder.gallery_title = convertView.findViewById<View>(R.id.gallery_title) as TextView

                convertView.tag = holder
            } else {
                holder = convertView.tag as AlbumViewHolder
            }
            holder.galleryImage!!.id = position
            holder.gallery_count!!.id = position
            holder.gallery_title!!.id = position

            var song: HashMap<String, String> = data[position]
            try {
                holder.gallery_title!!.text = song[f.KEY_ALBUM]
                holder.gallery_count!!.text = song[f.KEY_COUNT]

                Glide.with(activity)
                        .load(File(song[f.KEY_PATH])) // Uri of the picture
                        .into(holder.galleryImage!!)


            } catch (e: Exception) {
            }

            return convertView
        }
    }


    internal class AlbumViewHolder {
        var galleryImage: ImageView? = null
        var gallery_count: TextView? = null
        var gallery_title: TextView? = null
    }