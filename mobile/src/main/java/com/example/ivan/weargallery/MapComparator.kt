package com.example.ivan.weargallery

import java.util.Comparator
import java.util.HashMap

internal class MapComparator(private val key: String, private val order: String) : Comparator<HashMap<String, String>> {

    override fun compare(first: HashMap<String, String>,
                         second: HashMap<String, String>): Int {
        // TODO: Null checking, both for maps and values
        val firstValue = first[key]
        val secondValue = second[key]
        return if (this.order.toLowerCase().contentEquals("asc"))
            firstValue!!.compareTo(secondValue!!)
        else
            secondValue!!.compareTo(firstValue!!)

    }
}