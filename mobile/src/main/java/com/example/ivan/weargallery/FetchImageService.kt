package com.example.ivan.weargallery

import android.app.IntentService
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.util.Log
import com.bumptech.glide.Glide
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.wearable.Asset
import com.google.android.gms.wearable.PutDataMapRequest
import com.google.android.gms.wearable.Wearable
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*
import com.example.ivan.weargallery.Constants.TAG
import com.example.ivan.weargallery.Constants.WEAR_MESSAGE_PATH


class FetchImageService : IntentService("FetchImageService") {

    private var mGoogleApiClient: GoogleApiClient? = null

    private val randomUrl: String
        get() {
            var url = ""

            val min = 0
            val max = urls.size - 1

            if (max > 0) {
                val randomIndex = Random().nextInt(max - min + 1) + min
                url = urls[randomIndex]
            }

            Log.d(TAG, "Image url: $url")

            return url
        }

    override fun onHandleIntent(intent: Intent?) {
        Log.d(TAG, "OnHandleIntent start...")
        val url = randomUrl
        var bitmap: Bitmap? = null

        if (!url.isEmpty()) {
            bitmap = getBitmap(url)
        }

        if (bitmap == null) {
            //return default bitmap stored on device
            bitmap = (resources.getDrawable(R.drawable.violin) as BitmapDrawable).bitmap

            try {
                Log.d(TAG, "Sleeping for 1 second...")
                Thread.sleep(1000)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

        }

        //publish result, once image is downloaded
        publishImage(bitmap!!)
    }

    private fun publishImage(bitmap: Bitmap) {

        connectGoogleApiClient()
        val asset = createAssetFromBitmap(bitmap)
        val dataMap = PutDataMapRequest.create("/image")
        dataMap.dataMap.putAsset("profileImage", asset)
        dataMap.dataMap.putLong("timeStamp", Date().time) //to ensure data is changed everytime
        val request = dataMap.asPutDataRequest()
        val pendingResult = Wearable.DataApi
                .putDataItem(mGoogleApiClient, request)
        Log.d(TAG, "Bitmap sent to node...")

    }

    private fun connectGoogleApiClient() {
        //---Build a new Google API client---
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build()
        mGoogleApiClient!!.connect()
    }

    private fun getBitmap(url: String): Bitmap? {
        var bitmap: Bitmap? = null

        var any = try {
            //size for moto 360 v1
            bitmap = Glide.with(this).asBitmap().load(url).into(320, 290).get()
        } catch (e: IOException) {
            Log.e(TAG, "Error downloading image: " + e.message)
        }

        return bitmap
    }

    companion object {

        //TODO: add 1 or more image urls here
        private val urls = arrayOf(
                "http://vignette3.wikia.nocookie.net/goanimate-v2/images/7/77/Mrhappy0902_468x442.jpg"
                )

        private fun createAssetFromBitmap(bitmap: Bitmap): Asset {
            val byteStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream)
            return Asset.createFromBytes(byteStream.toByteArray())
        }
    }

}