package com.example.ivan.weargallery

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.wearable.activity.WearableActivity
import android.support.wearable.view.BoxInsetLayout
import android.util.Log
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.wearable.MessageApi
import com.google.android.gms.wearable.Wearable
import com.example.ivan.weargallery.Constants.IMAGE_RECEIVED_INTENT
import com.example.ivan.weargallery.Constants.TAG
import com.example.ivan.weargallery.Constants.WEAR_MESSAGE_PATH


class MainActivity : Activity(), MainView {

    private var mGoogleApiClient: GoogleApiClient? = null
    private var boxInsetLayout: BoxInsetLayout? = null

    // handler for received Intents for the IMAGE_RECEIVED_INTENT event
    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d(TAG, "On receive...")
            val bitmap = intent.extras!!.get("image") as Bitmap
            setBackground(bitmap)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "OnCreate...")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        boxInsetLayout = findViewById(R.id.boxInsetLayout) as BoxInsetLayout

        //---Build a new Google API client---
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build()
        mGoogleApiClient!!.connect()

        // Register mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter(IMAGE_RECEIVED_INTENT))
    }

    override fun onResume() {
        Log.d(TAG, "OnResume...")
        super.onResume()
        sendMessage(WEAR_MESSAGE_PATH, "fetchImage")
    }

    private fun sendMessage(path: String, text: String) {
        Thread(Runnable {
            val nodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await()
            for (node in nodes.nodes) {
                val result = Wearable.MessageApi.sendMessage(mGoogleApiClient, node.id,
                        path, text.toByteArray()).await()
                if (result.requestId == MessageApi.UNKNOWN_REQUEST_ID) {
                    Log.e(TAG, "Failed to send message: $text")
                }
            }
        }).start()
    }

    override fun setBackground(bitmap: Bitmap) {
        Log.d(TAG, "Set background...")
        val drawable = BitmapDrawable(resources, bitmap)
        boxInsetLayout!!.background = drawable
    }

    override fun onPause() {
        Log.d(TAG, "On pause...")
        mGoogleApiClient!!.disconnect()
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)
        super.onPause()
    }
}



