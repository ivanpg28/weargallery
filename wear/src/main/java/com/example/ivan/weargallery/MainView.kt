package com.example.ivan.weargallery

import android.graphics.Bitmap

interface MainView {
    fun setBackground(bitmap: Bitmap)
}