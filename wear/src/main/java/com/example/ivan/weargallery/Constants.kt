package com.example.ivan.weargallery

object Constants {

    val TAG = "WILT" //Wear Image Log Tag
    val WEAR_MESSAGE_PATH = "/image"
    val IMAGE_RECEIVED_INTENT = "ImageReceivedIntent"

}