package com.example.ivan.weargallery

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.wearable.*
import java.util.concurrent.TimeUnit
import com.example.ivan.weargallery.Constants.IMAGE_RECEIVED_INTENT
import com.example.ivan.weargallery.Constants.TAG
import com.example.ivan.weargallery.Constants.WEAR_MESSAGE_PATH

class DataLayerListenerService : WearableListenerService() {

    override fun onDataChanged(dataEvents: DataEventBuffer?) {
        Log.d(TAG, "Data changed...")
        for (event in dataEvents!!) {
            if (event.type == DataEvent.TYPE_CHANGED && event.dataItem.uri.path == WEAR_MESSAGE_PATH) {
                val dataMapItem = DataMapItem.fromDataItem(event.dataItem)
                val profileAsset = dataMapItem.dataMap.getAsset("profileImage")

                Log.d(TAG, "profile asset: " + profileAsset.toString())

                LoadImageTask().execute(*arrayOf(profileAsset))

            }
        }
    }

    private inner class LoadImageTask : AsyncTask<Asset, Void, Bitmap>() {
        private var mGoogleApiClient: GoogleApiClient? = null

        init {
            connectGoogleApiClient()
        }

        override fun doInBackground(vararg assets: Asset): Bitmap? {
            Log.d(TAG, "do in background...")
            var bitmap: Bitmap? = null
            if (assets.size > 0) {
                bitmap = loadBitmapFromAsset(assets[0])
            } else {
                Log.e(TAG, "assets length is 0")
            }

            return bitmap
        }

        private fun sendMessage(bitmap: Bitmap) {
            Log.d(TAG, "sending message to main...")
            val intent = Intent(IMAGE_RECEIVED_INTENT)
            intent.putExtra("image", bitmap)
            LocalBroadcastManager.getInstance(this@DataLayerListenerService).sendBroadcast(intent)
        }

        private fun connectGoogleApiClient() {
            //---Build a new Google API client---
            mGoogleApiClient = GoogleApiClient.Builder(this@DataLayerListenerService)
                    .addApi(Wearable.API)
                    .build()
            mGoogleApiClient!!.connect()
        }

        private fun loadBitmapFromAsset(asset: Asset?): Bitmap? {
            if (asset == null) {
                throw IllegalArgumentException("Asset must be non-null")
            }

            val result = mGoogleApiClient!!.blockingConnect(2000, TimeUnit.MILLISECONDS)

            if (!result.isSuccess) {
                return null
            }

            // convert asset into a file descriptor and block until it's ready
            val assetInputStream = Wearable.DataApi.getFdForAsset(
                    mGoogleApiClient, asset).await().inputStream
            mGoogleApiClient!!.disconnect()

            if (assetInputStream == null) {
                Log.e(TAG, "Requested an unknown Asset.")
                return null
            }

            // decode the stream into a bitmap
            return BitmapFactory.decodeStream(assetInputStream)
        }

        override fun onPostExecute(bitmap: Bitmap) {
            Log.d(TAG, "on post execute...")
            sendMessage(bitmap)
            mGoogleApiClient!!.disconnect()
        }
    }

}